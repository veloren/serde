use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct GenericTupleStruct<T, U>(T, U);
