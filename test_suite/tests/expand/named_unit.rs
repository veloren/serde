use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct NamedUnit;
